// fibonacci - prints the first N numbers of the sequence
package main

import "fmt"

// fibonacci is a function that returns
// a function that returns an int.
func fibonacci() func() int {
	var fib = map[int]int{0: 0, 1: 1, 2: 1}
	var index int = 0

	return func() int {
		v, ok := fib[index]
		if !ok {
			fib[index] = fib[index-1] + fib[index-2]
		}
		v = fib[index]
		index++
		return v
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
