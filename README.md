# fetch
Fetches the specified URLs and prints the output to stdout.

    This code is based on the similarly named program from the book
    "The Go Programming Language" by Alan Donovan, and Brian Kernighan.

## Sample Programs

**fetch usage**

``` bash
$ fetch <url> [url2 url3 ...]
```

